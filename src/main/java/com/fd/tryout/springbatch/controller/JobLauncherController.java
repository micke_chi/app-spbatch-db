package com.fd.tryout.springbatch.controller;

import com.fd.tryout.springbatch.batch.ToUpperCaseJobExecutionListener;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author furkand
 * 10/26/2018 6:51 PM
 */
@Slf4j
@RestController
@RequestMapping("/job")
public class JobLauncherController {

    private static final Logger LOG = LoggerFactory.getLogger(JobLauncherController.class);

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    @Qualifier("toUpperCaseJob")
    Job toUpperCaseJob;

    @Autowired
    JobRepository jobRepository;



    @GetMapping("/run")
    public String runJob() {
        try {
            LOG.info("Starting job...");
            JobParameters jobParameters = new JobParametersBuilder()
                    .addLong("time", System.currentTimeMillis())
                    .toJobParameters();
            //this.jobLauncher.run(this.toUpperCaseJob, jobParameters);
            JobExecution execution = this.asyncJobLauncher().run(this.toUpperCaseJob, jobParameters);
            LOG.info("Estatus job: " + execution.getStatus());
            LOG.info("Estatus job: " + execution.getAllFailureExceptions());

            return "DONE! " + execution.getStatus();
        }
        catch (Exception e) {
            LOG.error(e.getMessage());
        }

        return "DONE!";
    }

    @Bean
    public JobLauncher asyncJobLauncher(){
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(new SimpleAsyncTaskExecutor());
        return jobLauncher;
    }



    //@Scheduled(cron = "0/10 * * * * ?")
    @Scheduled(fixedRate = 3000)
    @Async
    public void perform() throws Exception
    {
        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(this.toUpperCaseJob, params);
    }









}
