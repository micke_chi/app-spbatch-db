package com.fd.tryout.springbatch.services;

import com.fd.tryout.springbatch.model.Prioridad;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PrioridadService {

    List<Prioridad> getPrioridades();

    Prioridad getPrioridad(Integer idprioridad);
}
