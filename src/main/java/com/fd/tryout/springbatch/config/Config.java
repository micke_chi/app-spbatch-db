package com.fd.tryout.springbatch.config;

import com.fd.tryout.springbatch.batch.*;
import com.fd.tryout.springbatch.model.Person;
import com.fd.tryout.springbatch.model.Prioridad;
import oracle.jdbc.OracleTypes;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStreamWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.StoredProcedureItemReader;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;

import javax.persistence.ParameterMode;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author furkand
 * 10/26/2018 5:57 PM
 */
@Configuration
@EnableBatchProcessing
public class Config {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private Step toUpperCaseConverter;

    @Bean
    public FlatFileItemReader<Person> reader() {
        return new FlatFileItemReaderBuilder<Person>()
                .name("personReader")
                .resource(new ClassPathResource("data.csv"))
                .delimited()
                .names(new String[]{"name", "surname"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }

    @Bean
    public StoredProcedureItemReader<Prioridad> spReader(DataSource dataSource){
        List<SqlParameter> parameters = new ArrayList<>();
        //"OUT_RESULTSET", void.class, ParameterMode.REF_CURSOR
        //parameters.add(new SqlOutParameter("OUT_RESULTSET", OracleTypes.CURSOR));

        StoredProcedureItemReader<Prioridad> reader = new StoredProcedureItemReader<>();
        reader.setDataSource(dataSource);
        reader.setProcedureName("MC.PKG_PRIORIZADOR_TAREAS.OBTENER_LISTADO_TAREAS");
        reader.setParameters(new SqlParameter[] { new SqlParameter("OUT_RESULTSET", OracleTypes.CURSOR) });
        reader.setRowMapper(new PrioridadMapper());
        reader.setRefCursorPosition(1);
        //reader.setPreparedStatementSetter(new PreparedStatementSetter);
        //reader.setVerifyCursorPosition(false);
        return reader;
    }


    /*
    @Bean
    public PersonProcessor processor() {
    return new PersonProcessor();
    }
    */

    @Bean
    public PrioridadProcessor processor() {
        return new PrioridadProcessor();
    }

    /*@Bean
    public PersonStreamWriter writer() {
        return new PersonStreamWriter();
    }*/

    @Bean
    public PrioridadStreamWriter writer() {
        return new PrioridadStreamWriter();
    }

    @Bean
    public ToUpperCaseJobExecutionListener listener() {
        return new ToUpperCaseJobExecutionListener();
    }

    @Bean("toUpperCaseJob")
    public Job convertToUpperCaseJob(ToUpperCaseJobExecutionListener listener) {
        return this.jobBuilderFactory.get("toUpperCaseJob")
                .listener(listener)
                .flow(this.toUpperCaseConverter)
                .end()
                .build();
    }

    @Bean
    public Step toUpperCaseConverter() {
        return this.stepBuilderFactory.get("toUpperCase")
                .<Prioridad, Prioridad>chunk(10)
                .reader(this.spReader(dataSource))
                .processor(this.processor())
                .writer(this.writer())
                .build();
    }


}
