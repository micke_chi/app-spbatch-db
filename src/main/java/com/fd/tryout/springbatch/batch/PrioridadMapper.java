package com.fd.tryout.springbatch.batch;

import com.fd.tryout.springbatch.model.Prioridad;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PrioridadMapper implements RowMapper<Prioridad> {

    @Override
    public Prioridad mapRow(ResultSet rs, int i) throws SQLException {

        Prioridad prio =  new Prioridad();
        prio.setIdprio(rs.getInt(1));
        prio.setDescripcion(rs.getString(2));

        prio.setFechaRegistro(rs.getString(3));
        prio.setFechaInicio(rs.getString(4));
        prio.setFechaFin(rs.getString(5));
        prio.setIdprioridad(rs.getInt(6));
        prio.setEstatus(rs.getString(7));
        prio.setIdestatus(rs.getInt(8));
        prio.setMensajeRespuesta(rs.getString(9));
        prio.setExito(rs.getInt(10));

        return prio;
    }
}
