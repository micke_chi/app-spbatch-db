package com.fd.tryout.springbatch.batch;

import com.fd.tryout.springbatch.model.Person;
import com.fd.tryout.springbatch.model.Prioridad;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * @author furkand
 * 10/26/2018 6:04 PM
 */
@Slf4j
public class PrioridadProcessor implements ItemProcessor<Prioridad, Prioridad> {

    private static final Logger LOG = LoggerFactory.getLogger(PrioridadProcessor.class);

    @Override
    public Prioridad process(Prioridad prio) throws Exception {
        LOG.info("Converting person: " + prio.toString());
        Prioridad p = new Prioridad();
        p.setIdprio(prio.getIdprio());
        p.setDescripcion(prio.getDescripcion().toUpperCase());
        p.setIdprioridad(prio.getIdprioridad());
        p.setFechaRegistro(prio.getFechaRegistro());

        Thread.sleep(100);
        return p;
    }
}
