package com.fd.tryout.springbatch.batch;

import com.fd.tryout.springbatch.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * @author furkand
 * 10/26/2018 6:04 PM
 */
@Slf4j
public class PersonProcessor implements ItemProcessor<Person, Person> {

    private static final Logger LOG = LoggerFactory.getLogger(PersonProcessor.class);


    @Override
    public Person process(Person person) throws Exception {
        LOG.info("Converting person: " + person.toString());
        Person p = new Person(person.getName().toUpperCase(), person.getSurname().toUpperCase());
        Thread.sleep(1000);
        return p;
    }
}
