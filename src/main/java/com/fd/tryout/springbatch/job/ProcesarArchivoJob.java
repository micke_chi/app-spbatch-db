package com.fd.tryout.springbatch.job;

import com.fd.tryout.springbatch.batch.PersonProcessor;
import com.fd.tryout.springbatch.batch.PersonStreamWriter;
import com.fd.tryout.springbatch.batch.ProcesarArchivoExecutionListener;
import com.fd.tryout.springbatch.model.Person;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

public class ProcesarArchivoJob {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private Step toUpperCaseConverter;

    @Autowired
    private Step leerArchivoStep;


    @Bean
    public FlatFileItemReader<Person> reader(Integer tipoArchivo) {
        String nombreArchivo = "";
        String nombreReader = "";
        if(tipoArchivo == 1){
            nombreArchivo = "data1.csv";
            nombreReader = "personReader1";
        }else{
            nombreArchivo = "data2.csv";
            nombreReader = "personReader2";
        }

        return new FlatFileItemReaderBuilder<Person>()
                .name(nombreArchivo)
                .resource(new ClassPathResource(nombreReader))
                .delimited()
                .names(new String[]{"name", "surname"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }


    @Bean
    public PersonProcessor processor() {
        return new PersonProcessor();
    }

    @Bean
    public PersonStreamWriter writer() {
        return new PersonStreamWriter();
    }


    @Bean
    public ProcesarArchivoExecutionListener listener() {
        return new ProcesarArchivoExecutionListener();
    }


    @Bean("procesarArchivoUnoJob")
    public Job procesarArchivoUnoJob(ProcesarArchivoExecutionListener listener) {
        return this.jobBuilderFactory.get("procesarArchivoUnoJob")
                .listener(listener)
                .flow(this.toUpperCaseConverter)
                .end()
                .build();
    }

    @Bean("procesarArchivoDosJob")
    public Job procesarArchivoDosJob(ProcesarArchivoExecutionListener listener) {
        return this.jobBuilderFactory.get("procesarArchivoDosJob")
                .listener(listener)
                .flow(this.leerArchivoStep)
                .end()
                .build();
    }

    @Bean
    public Step leerArchivoStep() {
        return this.stepBuilderFactory.get("leerArchivoStep")
                .<Person, Person>chunk(10)
                .reader(this.reader(2))
                .processor(this.processor())
                .writer(this.writer())
                .build();
    }


}
