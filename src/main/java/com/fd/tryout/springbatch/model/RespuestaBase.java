package com.fd.tryout.springbatch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;

public class RespuestaBase {
	
	@JsonProperty("mensaje_respuesta")
	@Column(name = "MENSAJE_RESPUESTA")
	private String mensaje_respuesta;
	
	@JsonProperty("exito")
	@Column(name = "EXITO")
	private Integer exito;
	
	

	public RespuestaBase(String mensaje_respuesta, Integer exito) {
		super();
		this.mensaje_respuesta = mensaje_respuesta;
		this.exito = exito;
	}

	public RespuestaBase() {
		super();
		this.mensaje_respuesta = "";
		this.exito = 0;
	}

	public String getMensaje_respuesta() {
		return mensaje_respuesta;
	}

	public void setMensaje_respuesta(String mensaje_respuesta) {
		this.mensaje_respuesta = mensaje_respuesta;
	}

	public Integer getExito() {
		return exito;
	}

	public void setExito(Integer exito) {
		this.exito = exito;
	}
	

}
