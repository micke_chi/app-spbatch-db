package com.fd.tryout.springbatch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@SqlResultSetMapping(name = "ModeloMapping", classes = {
		@ConstructorResult(targetClass = Modelo.class, columns = {
				@ColumnResult(name = "idmodelo", type = Integer.class),
				@ColumnResult(name = "modelo", type = String.class),
				@ColumnResult(name = "descripcion", type = String.class),
				@ColumnResult(name = "idclase", type = Integer.class),
				@ColumnResult(name = "idsubclase", type = Integer.class),
				@ColumnResult(name = "iddepto", type = Integer.class),
				@ColumnResult(name = "numpro", type = Integer.class),
				@ColumnResult(name = "mensaje_respuesta", type = String.class),
				@ColumnResult(name = "exito", type = Integer.class)
				}) 
		})
public class Modelo extends RespuestaBase{

	@Id
	@JsonProperty("idmodelo")
	private Integer idmodelo;
	
	@JsonProperty("modelo")
	private String modelo;
	
	@JsonProperty("descripcion")
	private String descripcion;
	
	@JsonProperty("idclase")
	private Integer idclase;
	
	@JsonProperty("idsubclase")
	private Integer idsubclase;
	
	@JsonProperty("iddepto")
	private Integer iddepto;
	
	@JsonProperty("numpro")
	private Integer numpro;
	
			
	public Modelo(Integer idmodelo, String modelo, String descripcion, Integer idclase, Integer idsubclase,
                  Integer iddepto, Integer numpro, String mensaje_respuesta, Integer exito) {
		super(mensaje_respuesta, exito);
		this.idmodelo = idmodelo;
		this.modelo = modelo;
		this.descripcion = descripcion;
		this.idclase = idclase;
		this.idsubclase = idsubclase;
		this.iddepto = iddepto;
		this.numpro = numpro;
	}

	public Modelo() {}


	public Integer getIdmodelo() {
		return idmodelo;
	}


	public void setIdmodelo(Integer idmodelo) {
		this.idmodelo = idmodelo;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Integer getIdclase() {
		return idclase;
	}


	public void setIdclase(Integer idclase) {
		this.idclase = idclase;
	}


	public Integer getIdsubclase() {
		return idsubclase;
	}


	public void setIdsubclase(Integer idsubclase) {
		this.idsubclase = idsubclase;
	}


	public Integer getIddepto() {
		return iddepto;
	}


	public void setIddepto(Integer iddepto) {
		this.iddepto = iddepto;
	}


	public Integer getNumpro() {
		return numpro;
	}


	public void setNumpro(Integer numpro) {
		this.numpro = numpro;
	}
	
	
	
}
