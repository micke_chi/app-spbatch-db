package com.fd.tryout.springbatch.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@SqlResultSetMapping(name = "PrioridadMapping", classes = {
        @ConstructorResult(targetClass = Modelo.class, columns = {
                @ColumnResult(name = "idprio", type = Integer.class),
                @ColumnResult(name = "descripcion", type = String.class),
                @ColumnResult(name = "fechaRegistro", type = String.class),
                @ColumnResult(name = "fechaInicio", type = String.class),
                @ColumnResult(name = "fechaFin", type = String.class),
                @ColumnResult(name = "idprioridad", type = Integer.class),
                @ColumnResult(name = "estatus", type = String.class),
                @ColumnResult(name = "idestatus", type = Integer.class),
                @ColumnResult(name = "mensaje_respuesta", type = String.class),
                @ColumnResult(name = "exito", type = Integer.class)
        })
})
public class Prioridad {

    @Id
    @JsonProperty("idprio")
    private int idprio;

    @JsonProperty("descripcion")
    private String descripcion;

    @JsonProperty("modelo")
    private String fechaRegistro;

    @JsonProperty("fechaInicio")
    private String fechaInicio;

    @JsonProperty("fechaFin")
    private String fechaFin;

    @JsonProperty("idprioridad")
    private int idprioridad;

    @JsonProperty("estatus")
    private String estatus;

    @JsonProperty("idestatus")
    private int idestatus;

    @JsonProperty("mensajeRespuesta")
    private String mensajeRespuesta;

    @JsonProperty("exito")
    private int exito;


    public Prioridad (Integer idprio, String descripcion, String fechaRegistro, String fechaInicio, String fechaFin, Integer idprioridad, String estatus, Integer idestatus, String mensajeRespuesta, Integer exito) {
        this.idprio = idprio;
        this.descripcion = descripcion;
        this.fechaRegistro = fechaRegistro;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.idprioridad = idprioridad;
        this.estatus = estatus;
        this.idestatus = idestatus;
        this.mensajeRespuesta = mensajeRespuesta;
        this.exito = exito;
    }

    public Prioridad(){}

    public int getIdprio() {
        return idprio;
    }

    public void setIdprio(int idprio) {
        this.idprio = idprio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public int getIdprioridad() {
        return idprioridad;
    }

    public void setIdprioridad(int idprioridad) {
        this.idprioridad = idprioridad;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public int getIdestatus() {
        return idestatus;
    }

    public void setIdestatus(int idestatus) {
        this.idestatus = idestatus;
    }

    public String getMensajeRespuesta() {
        return mensajeRespuesta;
    }

    public void setMensajeRespuesta(String mensajeRespuesta) {
        this.mensajeRespuesta = mensajeRespuesta;
    }

    public int getExito() {
        return exito;
    }

    public void setExito(int exito) {
        this.exito = exito;
    }

    @Override
    public String toString() {
        return "Prioridad{" +
                "idprio=" + idprio +
                ", descripcion='" + descripcion + '\'' +
                ", fechaRegistro='" + fechaRegistro + '\'' +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaFin='" + fechaFin + '\'' +
                ", idprioridad=" + idprioridad +
                ", estatus='" + estatus + '\'' +
                ", idestatus=" + idestatus +
                ", mensajeRespuesta='" + mensajeRespuesta + '\'' +
                ", exito=" + exito +
                '}';
    }
}
